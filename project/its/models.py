from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import User
from its.account.models import Group
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

CONTENT_TYPE = (
    ('folder', '폴더'),
    ('practice', '이론'),
    ('exercise','이론문제'),
    ('problem', '문제'),
    
)

class Content(MPTTModel):

    
    group = models.ForeignKey(Group)
    type = models.CharField(max_length=16,choices=CONTENT_TYPE) 
    order = models.IntegerField()
    title = models.CharField(max_length=254)
    body = RichTextUploadingField()
    code = models.TextField(null=True, blank=True)
    input = models.TextField(null=True, blank=True)
    answer = models.TextField(null=True, blank=True)
    hint = models.TextField(null=True, blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    
    class MPTTMeta:
        order_insertion_by=['order']
        
    def __str__(self):
        return self.title

    