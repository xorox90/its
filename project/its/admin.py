from django.contrib import admin
from django.contrib.auth.models import User
from its.board.models import Board,Post
from .models import Content
from its.account.models import *
from mptt.admin import MPTTModelAdmin

class SolvedAdmin(admin.ModelAdmin):

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if(request.user.is_superuser):
            return super(SolvedAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
            
        if(db_field.name=='user'):
            kwargs["queryset"] = User.objects.filter(itsuser__group=request.user.itsuser.group)
        elif(db_field.name=='content'):
            kwargs["queryset"] = Content.objects.filter(group=request.user.itsuser.group)
        return super(SolvedAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
        
        
    def get_queryset(self, request):
        qs = super(SolvedAdmin, self).get_queryset(request)
        if(request.user.is_superuser):
            return qs
            
        user=User.objects.filter(itsuser__group=request.user.itsuser.group)
        return qs.filter(user=user)
        
        
class ITSUserAdmin(admin.ModelAdmin):

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if(request.user.is_superuser):
            return super(ITSUserAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
            
        if(db_field.name=='parent'):
            kwargs["queryset"] = Content.objects.filter(group=request.user.itsuser.group)
        elif(db_field.name=='group'):
            kwargs["queryset"] = Group.objects.filter(id=request.user.itsuser.group.id)
        return super(ITSUserAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
        
    def get_queryset(self, request):
        qs = super(ITSUserAdmin, self).get_queryset(request)
        if(request.user.is_superuser):
            return qs
        return qs.filter(group=request.user.itsuser.group)
        
class UserAdmin(admin.ModelAdmin):

    def has_module_permission(self,request):
        return True

    def get_queryset(self, request):
        qs = super(UserAdmin, self).get_queryset(request)
        if(request.user.is_superuser):
            return qs
        return qs.filter(itsuser__group=request.user.itsuser.group)

class ContentAdmin(MPTTModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if(request.user.is_superuser):
            return super(ContentAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
            
        if(db_field.name=='parent'):
            kwargs["queryset"] = Content.objects.filter(group=request.user.itsuser.group)
        elif(db_field.name=='group'):
            kwargs["queryset"] = Group.objects.filter(id=request.user.itsuser.group.id)
        return super(ContentAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
        
    def get_queryset(self, request):
        qs = super(ContentAdmin, self).get_queryset(request)
        if(request.user.is_superuser):
            return qs
        return qs.filter(group=request.user.itsuser.group)
        
        
    
# Register your models here.

#admin.site.register(Board)
#admin.site.register(Post)
#admin.site.unregister(User)
#admin.site.register(User,UserAdmin)
admin.site.register(ITSUser,ITSUserAdmin)
admin.site.register(Solved, SolvedAdmin)
admin.site.register(Content,ContentAdmin)