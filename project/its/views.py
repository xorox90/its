#-*- coding :utf8 -*-
from django.shortcuts import render,redirect, get_object_or_404
from django.http import HttpResponse
from django.template import RequestContext, loader

from .models import Content
from its.account.models import Solved
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
import os
# Create your views here.

def compile(source,input):
    import subprocess, os
    import random, string
    import time
    from subprocess import TimeoutExpired
    from subprocess import PIPE


    def randword(length):
       return ''.join(random.choice(string.digits) for i in range(length))

    rand_filename=os.path.join(settings.MEDIA_ROOT,"compile/")+randword(8)+'.out'
    rand_input=os.path.join(settings.MEDIA_ROOT,"compile/")+randword(8)+'.c'
    
    f=open(rand_input,'w')
    f.write(source)
    f.close();
    
    proc = subprocess.Popen('gcc -std=c99 -o %s %s'%(rand_filename, rand_input), stdout=PIPE, stderr=PIPE,shell=True,universal_newlines=True)
    
    
    try:
        outs, errs = proc.communicate(timeout=15)
        proc.wait()
    except TimeoutExpired:
        proc.kill()
        return(None,"compile timeout")
    finally:
        os.remove(rand_input)

    if(errs):
        return(None,errs)
    
    proc = subprocess.Popen(rand_filename, stdout=PIPE, stderr=PIPE,stdin=PIPE,universal_newlines=True)
    try:
        out,errs=proc.communicate(input,timeout=15)
        proc.wait()
    except TimeoutExpired:
        proc.kill()
        return(None,"timeout")  
    finally:
        os.remove(rand_filename)
   
    if errs:
        return (None,errs) 
    else:
        return(out,None)

@login_required
def problem(request,id=None):
    template = loader.get_template('its/problem.html')
    
    if id:
        content=get_object_or_404(Content,id=id,group=request.user.itsuser.group)
    else:
        content=None
    
    error=None

    out=None
    correct=False

    if(request.POST):
        if(content.type=='practice'):
            correct=True
        else:
            out,error=compile(request.POST['code'], content.input)
            if(error):
                pass
            elif(out):
                print(out,content.answer)
                if(out==content.answer):
                    out="정답입니다!"
                    correct=True
                else:
                    error="정답과 일치하지않습니다."
    
    if(correct):
        try:
            Solved.objects.get(user=request.user, content=content)
        except ObjectDoesNotExist:
            solved=Solved(user=request.user, content=content,correct=True)
            solved.save() 
                
            
    indexes = Content.objects.filter(group=request.user.itsuser.group)
    
    
    for index in indexes:
        try:
            solved=Solved.objects.get(user=request.user, content=index)
            if(solved and solved.correct):
                index.correct=True
                
        except ObjectDoesNotExist:
            index.correct=False

    try:
        solved=Solved.objects.get(user=request.user, content=content)
    except ObjectDoesNotExist:
        solved = None
    
        
    
    context = RequestContext(request, {
        'index':indexes,
        'content':content,
        'solved':solved,
        'error':error,
        'out':out,
    })
    
    
    return HttpResponse(template.render(context))