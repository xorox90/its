from django import forms
from django.db import models
from .models import ITSUser
from captcha.fields import CaptchaField

class BaseSignUpForm(forms.Form):
    username = forms.CharField(max_length=254)
    password = forms.CharField(widget=forms.PasswordInput)
    question = forms.CharField(max_length=254)
    question_answer = forms.CharField(max_length=254)
    
    
class StudentSignUpForm(BaseSignUpForm):
    teacher = forms.CharField(label="Teacher Username")
    captcha = CaptchaField()
    
class TeacherSignUpForm(BaseSignUpForm):
    captcha = CaptchaField()

class SearchID(forms.Form):
    username = forms.CharField(max_length=254)
    
class Searchpassword(forms.Form):
    question = forms.CharField(max_length=254)
    question_answer = forms.CharField(max_length=254)
    
class Settingpassword(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)
    verity = forms.CharField(widget=forms.PasswordInput)
    

  
