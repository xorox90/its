#-*- coding :utf8 -*-
from django.shortcuts import render,redirect, get_object_or_404
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *
from its.board.models import *
from its.models import *

from .forms import *

def searchID(request):
    template = loader.get_template('registration/searchID.html')
    
    if request.method == 'POST':
        form = SearchID(request.POST)
        if form.is_valid():
            user = User.objects.get(username=form.cleaned_data['username'])
            if(user):
               return redirect('/accounts/searchpassword?id=%s'%user.itsuser.user)
            else:
               return redirect('/accounts/searchID')

    else:
        form = SearchID()

    context = RequestContext(request, {
    'form': form
        
    })
    return HttpResponse(template.render(context))
    
def searchpassword(request):
    template = loader.get_template('registration/searchpassword.html')
    username = request.GET['id']
    user = User.objects.get(username=username)
    
    if request.method == 'POST':
        form = Searchpassword(request.POST)
        if form.is_valid():
            question_answer=request.POST['question_answer']
            if(user.itsuser.question_answer == question_answer):
               return redirect('/accounts/settingpassword?id=%s'%user.itsuser.user)
            else:
               return redirect('/accounts/searchID')

    else:
        form = Searchpassword({'question':user.itsuser.question})

    context = RequestContext(request, {
    'form': form
        
    })
    return HttpResponse(template.render(context))

def settingpassword(request):
    template = loader.get_template('registration/settingpassword.html')
    username = request.GET['id']
    user = User.objects.get(username=username)
    
    if request.method == 'POST':
        form = Settingpassword(request.POST)
        if form.is_valid():
            password=request.POST['password']
            verity=request.POST['verity']
            if(password == verity):
                user.set_password(form.cleaned_data['password'])
                user.save()
                return redirect('/problem')


    else:
        form = Settingpassword()
        

    context = RequestContext(request, {
    'form': form
        
    })
    return HttpResponse(template.render(context))
    
    
def signup(request):
    template = loader.get_template('registration/signup_choose.html')
    

    if request.method == 'POST':
        form = StudentSignUpForm(request.POST)
        if form.is_valid():
            pass
    else:
        form = StudentSignUpForm()
        
    context = RequestContext(request, {
    'form': form
        
    })
    return HttpResponse(template.render(context))

def signupTeacher(request):
    template = loader.get_template('registration/signup.html')
    
    if request.method == 'POST':
        form = TeacherSignUpForm(request.POST)
        if form.is_valid():
             user=User(username=form.cleaned_data['username'])
             user.set_password(form.cleaned_data['password'])
             user.is_staff=True
             user.save()
             from django.contrib.auth.models import Permission
             from django.contrib.contenttypes.models import ContentType
             objs = [Post,Content,Solved,ITSUser]
             for obj in objs:
                contentType = ContentType.objects.get_for_model(obj)
                permissions = Permission.objects.filter(content_type=contentType)
                for permission in permissions:
                    user.user_permissions.add(permission)
     
             
             group=Group()
             group.save()
             
             board = Board(group=group)
             board.save()
             
             question=request.POST['question']
             question_answer=request.POST['question_answer']
             
             itsuser=ITSUser(user=user, type='T',group=group, question=question, question_answer=question_answer)
             itsuser.save()
             return redirect('/problem')

    else:
        form = TeacherSignUpForm()
        
    context = RequestContext(request, {
    'form': form
        
    })
    return HttpResponse(template.render(context))
    
def signupStudent(request):
    template = loader.get_template('registration/signup.html')
    

    if request.method == 'POST':
        form = StudentSignUpForm(request.POST)
        if form.is_valid():
            teacher = User.objects.get(username=form.cleaned_data['teacher'])
            if(teacher):
                print(teacher.itsuser.type)
                if(teacher.itsuser.type == 'T'):
                    user=User(username=form.cleaned_data['username'])
                    user.set_password(form.cleaned_data['password'])
                    user.save()
                    
                    question=request.POST['question']
                    question_answer=request.POST['question_answer']
                    
                    
                    itsuser=ITSUser(user=user, type='S',group=teacher.itsuser.group, question=question, question_answer=question_answer)
                    itsuser.save()
                    return redirect('/problem')
                else:
                    pass
            else:
                pass
    else:
        form = StudentSignUpForm()
        
    context = RequestContext(request, {
    'form': form
        
    })
    return HttpResponse(template.render(context))

