from django.db import models
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey

ITSUSER_CHOICE = (('S','학생'),('T','교사'))

class Group(models.Model):
    pass
    
class ITSUser(models.Model):
    user = models.OneToOneField(User)
    group = models.ForeignKey(Group)
    question = models.CharField(max_length=254)
    question_answer = models.CharField(max_length=254)
    type = models.CharField(max_length=16, choices=ITSUSER_CHOICE)   
    
    def __str__(self):
        return self.user.username

class Solved(models.Model):
    user = models.ForeignKey(User)
    content = models.ForeignKey('its.Content')
    correct = models.BooleanField()
    time=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return '[%s]%s'%(self.user.username,self.content.title)
