#-*- coding :utf8 -*-
from django.shortcuts import render,redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden
from django.template import RequestContext, loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import *
from .forms import PostForm
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def list(request):
    template = loader.get_template('its/board/list.html')
    
    board=request.user.itsuser.group.board
    all_posts=Post.objects.filter(board=board, parent=None).order_by('-time')
    paginator = Paginator(all_posts, 25)
    
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)

    context = RequestContext(request, {
        'posts' : posts,
        
    })
    return HttpResponse(template.render(context))
@login_required
def detail(request,id):
    template = loader.get_template('its/board/detail.html')
    board=request.user.itsuser.group.board
    post=Post.objects.get(board=board, id=id)
    
    if(post==None):
        return HttpResponseForbidden()
        
    
    replies=Post.objects.filter(parent=id).order_by('time')
    
    if(post==None):
        return redirect('/board')
    

    context = RequestContext(request, {
        'post' : post,
        'replies' : replies
        
    })
    return HttpResponse(template.render(context))
@login_required
def delete(request, id=None):
    board=request.user.itsuser.group.board
    post=get_object_or_404(Post,board=request.user.itsuser.group.board,id=id)
    
    if(post.user != request.user and request.user.itsuser.type!='T'):
        return HttpResponseForbidden()
        
    if(post.parent==None):
        replies=Post.objects.filter(parent=post.id)
        replies.delete()
        post.delete()
        return redirect('/board')
    else:
        post.delete()
        return redirect('/board/detail/%s'%(post.parent.id))
    
@login_required
def modify(request,id=None):
    template = loader.get_template('its/board/modify.html')
    
    board=request.user.itsuser.group.board
    post=get_object_or_404(Post,id=id, board=board)
    
    if(post.user != request.user and request.user.itsuser.type!='T'):
        return HttpResponseForbidden()
    
    rid=id
    if(post.parent):
        rid=post.parent.id

    if(request.POST):
        form=PostForm(request.POST,instance=post)
        post=form.save(commit=False)
        post.user=request.user
        post.board=board
        post.save()
        return redirect('/board/detail/%s'%(rid))

    form=PostForm(instance=post)
    
    context = RequestContext(request, {
        'form':form,
        'post':post,
        'rid':rid,     
    })
    return HttpResponse(template.render(context))
    
@login_required
def write(request,id=None):
    template = loader.get_template('its/board/write.html')
    
    if(request.POST):
        title=request.POST['title']
        body=request.POST['body']
        
        form=PostForm(request.POST)
        post=form.save(commit=False)
        
        post.board=request.user.itsuser.group.board
        post.user=request.user
        if(id):
            post.parent=Post.objects.get(board=post.board,id=id)
        else:
            post.parent=None
            
        post.save()
        
        return redirect('/board')
    
    form=PostForm()
    
    context = RequestContext(request, {
    'form':form
        
    })
    return HttpResponse(template.render(context))
    