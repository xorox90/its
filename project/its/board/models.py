from django.db import models
from django.contrib.auth.models import User
from its.account.models import Group
from ckeditor_uploader.fields import RichTextUploadingField
class Board(models.Model):
    group=models.OneToOneField(Group)
    
    

class Post(models.Model):
    title=models.CharField(max_length=255)
    body=RichTextUploadingField()
    time=models.DateTimeField(auto_now_add=True)
    parent=models.ForeignKey('self',null=True, blank=True)
    user=models.ForeignKey(User)
    board=models.ForeignKey(Board)
    