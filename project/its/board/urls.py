"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Homex
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^detail/(?P<id>\d+)', 'its.board.views.detail'),
    url(r'^modify/(?P<id>\d+)', 'its.board.views.modify'),
    url(r'^reply/(?P<id>\d+)', 'its.board.views.write'),
    url(r'^write/(?P<id>\d+)', 'its.board.views.write'),
    url(r'^write', 'its.board.views.write'),    
    url(r'^delete/(?P<id>\d+)', 'its.board.views.delete'),
    url(r'^', 'its.board.views.list'),

]
