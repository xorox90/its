"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import *

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    #https://docs.djangoproject.com/en/1.8/intro/tutorial03/
    url(r'^',include('its.urls')),
    url('^accounts/', include('django.contrib.auth.urls')),
    url('^accounts/signup$', 'its.account.views.signup'),
    url('^accounts/searchpassword$', 'its.account.views.searchpassword'),
    url('^accounts/settingpassword$', 'its.account.views.settingpassword'),
    url('^accounts/searchID$', 'its.account.views.searchID'),
    url('^accounts/signup/student', 'its.account.views.signupStudent'),
    url('^accounts/signup/teacher', 'its.account.views.signupTeacher'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^captcha/', include('captcha.urls')),

    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
